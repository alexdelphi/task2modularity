﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2Modularity.RandomGenerators;

namespace task2Modularity
{
    /// <summary>
    /// Модуль системы.
    /// </summary>
    class Module
    {
        /// <summary>
        /// Тип модуля.
        /// </summary>
        public int Type { get; private set; }
        /// <summary>
        /// Количество свободных ресурсов данного модуля.
        /// </summary>
        public int FreeResources { get; private set; }
        /// <summary>
        /// Занят ли модуль?
        /// </summary>
        public bool Occupied
        {
            get
            {
                return FreeResources == 0;
            }
        }
        /// <summary>
        /// Освобождает модуль
        /// </summary>
        public void Free()
        {
            FreeResources++;
        }
        public Module(int type, int freeResources)
        {
            Type = type;
            FreeResources = freeResources;
        }

        /// <summary>
        /// Занимает модуль
        /// </summary>
        public void Occupy()
        {
            if (Occupied)
            {
                throw new InvalidOperationException("Нет доступных модулей");
            }
            FreeResources--;
        }
    }
}
