﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// TODO client.Processed не вызывается
namespace task2Modularity
{
    /// <summary>
    /// Класс "Отложенного наблюдателя".
    /// Содержит коллекцию элементов, производящих действие в случайный тик.
    /// Интервал между тиками определяется распределением
    /// </summary>
    public class DeferredEventCollection
    {
        private readonly LinkedList<DeferredEvent> _events = new LinkedList<DeferredEvent>();
        public void Add(DeferredEvent subscriber)
        {
            _events.AddLast(subscriber);
        }
        public void Remove(DeferredEvent subscriber)
        {
            _events.Remove(subscriber);
        }
        /// <summary>
        /// Удалить всех подписчиков, соответствующих определенному условию
        /// </summary>
        /// <param name="pred"></param>
        public void RemoveAll(Predicate<DeferredEvent> pred)
        {
            _events.RemoveAll(pred);
        }
        public void Tick()
        {
            for (var node = _events.First; node != null; node = node.Next)
            {
                node.Value.Tick();
            }
            // Сносим все однократные события
            _events.RemoveAll(node => (node.Ticked && node.OccurrencePattern == OccurrencePattern.JustOnce));
        }
    }
}
