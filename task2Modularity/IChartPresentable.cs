﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1Modelling
{
    /// <summary>
    /// Позволяет показать данные на графике
    /// </summary>
    public interface IChartPoint
    {
        /// <summary>
        /// Получает подпись к точке
        /// </summary>
        /// <returns></returns>
        string GetChartLabel();
        double GetChartValue();
    }
}
