﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using MyTimer = System.Timers.Timer;

namespace task2Modularity
{
    /// <summary>
    /// Текстовое поле, принимающее только числа типа double
    /// </summary>
    public partial class NumericTextBox : TextBox
    {
        private double _internalValue;
        private const int _defaultValueCheckTime = 500;
        private MyTimer _timer = new MyTimer();
        /// <summary>
        /// Интервал между проверкой корректности ввода в миллисекундах.
        /// Слишком малое значение приводит к невозможности ввести дробные числа.
        /// Слишком большое - к "лагам" при обновлении значений.
        /// </summary>
        [DefaultValue(_defaultValueCheckTime)]
        public int ValueCheckTime { get; set; }
        public double Value
        {
            get 
            {
                return _internalValue;
            }
            set
            {
                _internalValue = value;                
                Text = value.ToString();
            }
        }
        private double _oldValue;
        public System.Windows.Forms.AutoScaleMode AutoScaleMode;
        public NumericTextBox()
        {
            InitializeComponent();
            ValueCheckTime = _defaultValueCheckTime;
            _timer.Interval = ValueCheckTime;
        }
        protected override void OnTextChanged(EventArgs e)
        {
            if (ValueCheckTime == 0)
            {
                return;
            }
            ElapsedEventHandler handler = null;
            handler = (s, args) => this.Invoke(new Action(() =>
            {
                try
                {
                    Value = Convert.ToDouble(Text);
                    _oldValue = Value;
                }
                catch (FormatException)
                {
                    Value = _oldValue;
                    Text = Value.ToString();
                }
                _timer.Elapsed -= handler;
                _timer.Stop();
            }));
            _timer.Elapsed += handler;
            _timer.Start();
            base.OnTextChanged(e);
        }
    }
}
