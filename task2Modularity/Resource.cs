﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity
{
    /// <summary>
    /// Ресурс - группа модулей одинакового типа. Тип модулей называется типом ресурса.
    /// Второй параметр - (случайное) время, на которое ресурсы заняты
    /// </summary>
    class Resource
    {
        public readonly int Type; 
        public readonly RandomDistribution Distribution;
        public Resource(int resourceType, RandomDistribution distribution)
        {
            Type = resourceType;
            Distribution = distribution;
        }
    }
}
