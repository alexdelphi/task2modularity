﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2Modularity.RandomGenerators;

namespace task2Modularity
{
    /// <summary>
    /// Описывает действие, которое наступает через несколько серверных тиков после его задания.
    /// Оно может происходить однократно или периодически.
    /// </summary>
    public class DeferredEvent
    {
        /// <summary>
        /// Количество тиков до следующего события
        /// </summary>
        public double Ticks { get; private set; }
        private RandomDistribution _randomDistribution;
        /// <summary>
        /// Код, выполняющийся, когда количество тиков станет равно 0
        /// </summary>
        public Action Trigger { get; private set; }
        /// <summary>
        /// Выполняется ли событие однократно или периодически
        /// </summary>
        public OccurrencePattern OccurrencePattern;
        public DeferredEvent(RandomDistribution randomDistribution, Action trigger, OccurrencePattern occurrencePattern)
        {
            _randomDistribution = randomDistribution;
            RefreshTicks();
            Trigger = trigger;
            OccurrencePattern = occurrencePattern;
        }
        public bool Ticked { get; private set; }
        private void RefreshTicks()
        {
            Ticks = Convert.ToInt32(Math.Ceiling(_randomDistribution.NextDouble()));
            if (Logger.Instance.Enabled) // из-за рефлексии
            {
                Logger.Instance.WriteLine(string.Format(
                    "Deferred event: refreshed ticks for {0} with distribution = {1}",
                    Ticks,
                    _randomDistribution.GetType().ToString()
                ));
            }
        }
        public bool Tick()
        {
            Ticked = false;
            if (Ticks == 0)
            {
                RefreshTicks();
                Ticked = true;
                Trigger();
            }
            Ticks--;
            return Ticked;
        }
    }
}
