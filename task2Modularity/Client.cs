﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task2Modularity
{
    public enum ClientState
    {
        Newborn,    // только появился
        Waiting,    // в очереди
        Processing, // на серваке
        Succeeded,  // уже вышел
        Failed      // не обработан т.к. не дошла очередь
    }
    /// <summary>
    /// Возникает, когда клиент попытались перевести в неверное состояние
    /// </summary>
    public class WrongClientStateException : InvalidOperationException
    {
        public readonly ClientState OldState, NewState;
        private readonly int _clientId;
        public WrongClientStateException(ClientState oldState, ClientState newState, int clientId)
        {
            OldState = oldState;
            NewState = newState;
            _clientId = clientId;
            Logger.Instance.WriteLine(Message);
        }
        public override string Message
        {
            get
            {
                return string.Format("{0} Клиент {1} попытались перевести из состояния {2} в состояние {3}", base.Message, _clientId, OldState, NewState);
            }
        }
    };
    /// <summary>
    /// Класс клиента. Подсчитывает время, хранит свое состояние (в очереди/в обработке и т.д.),
    /// а также сигнализирует об окончании обработки очередного модуля.
    /// </summary>
    public class Client
    {
        // метод, получающий время в тиках и обновляющий старое значение
        private readonly Func<int> _methodGetServerTicks;
        // последнее и предпоследнее полученное значение времени
        private int _currentServerTicks, _oldServerTicks;
        
        /// <summary>
        /// Номер клиента в списке
        /// </summary>
        public readonly int Id;

        /// <summary>
        /// Общее время ожидания клиента в очереди
        /// </summary>
        public int TicksWaiting { get; private set; }
        /// <summary>
        /// Время выхода из системы.
        /// </summary>
        public int TicksExit { get; private set; }
        /// <summary>
        /// Получает текущее время и обновляет значения нового/старого времени
        /// </summary>
        private void RefreshTicks()
        {
            _oldServerTicks = _currentServerTicks;
            int value = _methodGetServerTicks();
            _currentServerTicks = value;
        }
        /// <summary>
        /// Индекс обрабатываемого модуля
        /// </summary>
        public int ModuleId { get; private set; }
        private readonly List<int> _ticksProcessing = new List<int>();
        /// <summary>
        /// Время обработки каждого ресурса в тиках
        /// </summary>
        public IReadOnlyList<int> TicksProcessing
        {
            get
            {
                return _ticksProcessing;
            }
        }
        private ClientState _state;
        /// <summary>
        /// Состояние клиента
        /// </summary>
        public ClientState State
        {
            get
            {
                return _state;
            }
            set
            {
                Logger.Instance.WriteLine(string.Format("Client {0} changes state: {1} -> {2}. Module: {3}", Id, _state, value, ModuleId));
                _state = value;
            }
        }        
        /// <summary>
        /// Получить сообщение о завершении обработки текущего модуля
        /// </summary>
        public void CurrentModuleFinished()
        {
            RefreshTicks();
            int ticks = _currentServerTicks - _oldServerTicks;            
            _ticksProcessing.Add(_currentServerTicks - _oldServerTicks);
            OnProcessed();
        }        
        /// <summary>
        /// Получить сообщение о завершении обработки всех модулей
        /// </summary>
        public void WorkFinished()
        {
            RefreshTicks();
            if (State == ClientState.Processing)
            {
                TicksExit = _currentServerTicks;
                OnFinished();
                State = ClientState.Succeeded;
            }
            else
            {
                throw new WrongClientStateException(State, ClientState.Succeeded, Id);
            }
        }

        /// <summary>
        /// Получить сообщение о выталкивании из очереди
        /// </summary>
        public void PulledFromQueue()
        {
            TicksWaiting += _currentServerTicks - _oldServerTicks;
            StartProcessing();
        }
        /// <summary>
        /// Получить сообщение о начале обработки (обновить значения тиков)
        /// </summary>
        public void StartProcessing()
        {
            RefreshTicks();
            State = ClientState.Processing;
        }
        public string GetDebugInfo()
        {
            var builder = new StringBuilder();
            builder.Append("-------------------");
            builder.Append(string.Format("Data on client № {0}", Id));
            builder.Append("Ticks processing: ");
            builder.Append(TicksProcessing.ToJSON());
            builder.Append(string.Format("Ticks waiting: {0}", TicksWaiting.ToString()));
            return builder.ToString();
        }
        /// <summary>
        /// Создает нового клиента
        /// </summary>
        /// <param name="id">Номер клиента</param>
        /// <param name="methodGetServerTicks">Метод, получающий глобальное время в тиках</param>
        public Client(int id, Func<int> methodGetServerTicks)
        {
            _methodGetServerTicks = methodGetServerTicks;
            Id = id;
            ModuleId = 0;
        }
        /// <summary>
        /// Возникает, когда клиент освободил ресурс
        /// </summary>        
        public event Action Processed;
        protected virtual void OnProcessed()
        {
            ModuleId++; // Текущий ресурс обработали
            Processed();
        }
        /// <summary>
        /// Возникает, когда клиент закончил обрвботку
        /// </summary>
        public event Action Finished;
        protected virtual void OnFinished()
        {
            Finished();
        }
    }
}
