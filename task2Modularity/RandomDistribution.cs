﻿using System;
using System.Collections.Generic;

namespace task2Modularity
{
    public abstract partial class RandomDistribution
    {
        protected Random _random = new Random();
        public abstract double FromEvenRandom(double num);
        public List<double> GetRandomizedList(int n)
        {
            var lst = new List<double>();
            for (int i = 0; i < n; i++)
            {
                lst.Add(NextDouble());
            }
            return lst;
        }
        /// <summary>
        /// Рандомное значение в соответствии с FromEvenRandom()
        /// </summary>
        /// <returns></returns>
        public virtual double NextDouble()
        {
            return FromEvenRandom(_random.NextDouble());
        }
        /// <summary>
        /// Рандомное целочисленное значение; получается округлением в большую сторону
        /// </summary>
        /// <returns></returns>
        public virtual int NextInt()
        {
            return Convert.ToInt32(Math.Ceiling(NextDouble()));
        }
    }
}
