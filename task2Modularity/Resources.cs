﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2Modularity.RandomGenerators;

namespace task2Modularity
{
    /// <summary>
    /// Ресурс системы
    /// </summary>
    class Resource
    {
        /// <summary>
        /// Тип ресурса.
        /// </summary>
        public int Type { get; private set; }
        /// <summary>
        /// Количество "частей" данного ресурса в системе.
        /// </summary>
        public int FreeParts { get; private set; }
        /// <summary>
        /// Занят ли ресурс?
        /// </summary>
        public bool Occupied
        {
            get
            {
                return FreeParts == 0;
            }
        }
        /// <summary>
        /// Освобождает ресурс
        /// </summary>
        public void Free()
        {
            FreeParts++;
        }
        public Resource(int type, int freeParts)
        {
            Type = type;
            FreeParts = freeParts;
        }

        /// <summary>
        /// Занимает ресурс
        /// </summary>
        public void Occupy()
        {
            if (Occupied)
            {
                throw new InvalidOperationException("Нет доступных ресурсов");
            }
            FreeParts--;
        }
    }
}
