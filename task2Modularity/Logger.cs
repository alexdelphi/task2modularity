﻿using System;
using System.Globalization;
using System.IO;
using System.Timers;

namespace task2Modularity
{
    /// <summary>
    /// Для записи отладочного хлама.
    /// Синглтон, чтобы не заморачиваться с закрытием
    /// </summary>
    public class Logger : IDisposable
    {
        private Timer _flushTimer = new Timer();
        public bool Enabled { get; set; }
        // инициализация
        private StreamWriter _writer;
        private const string LogDirectory = "debug logs";
        private static readonly Lazy<Logger> _instance = new Lazy<Logger>(() =>
        {
            // кастомный формат, чтобы винда сжевала его в качестве имени файла
            Logger logger = new Logger();
            string logFileName = DateTime.Now.ToString("dd-MM-yyyy HH-mm-ss");
            if (!Directory.Exists(LogDirectory))
                Directory.CreateDirectory(LogDirectory);
            logger.Enabled = true;
            logger._writer = new StreamWriter(Path.Combine(LogDirectory, string.Concat(logFileName, ".txt")));
            logger._flushTimer.Interval = 500;
            logger._flushTimer.Elapsed += (s, args) =>
            {
                logger.Flush();
            };
            logger._flushTimer.Start();
            return logger;
        });
        public static Logger Instance
        {
            get
            {
                return _instance.Value;
            }
        }
        public void Flush()
        {
            try
            {
                _writer.Flush();
            }
            catch (ObjectDisposedException)
            {
                ;
            }
        }
        public void WriteLine(string message)
        {
            if (!Enabled)
            {
                return;
            }
            // прикрепляем спереди дату/время и пишем
            try
            {
                string currentTime = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss,fff");
                _writer.WriteLine(string.Format("{0} {1}", currentTime, message));
            }
            catch (ObjectDisposedException)
            {
                ; // ну не записали, бывает
            }
        }

        public void Dispose()
        {
            _writer.Close();
            _writer.Dispose();
        }
    }
}
