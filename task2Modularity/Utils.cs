﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task2Modularity
{
    public static class Utils
    {
        /// <summary>
        /// Удалить все элементы из связного списка, удовлетворяющие некоторому условию
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="match"></param>
        /// <returns></returns>
        public static int RemoveAll<T>(this LinkedList<T> list, Predicate<T> match) {
            var count = 0;
            var node = list.First;
            while (node != null) {
                var next = node.Next;
                if (match(node.Value)) {
                    list.Remove(node);
                    count++;
                }
                node = next;
            }
            return count;
        }
        /// <summary>
        /// Преобразует список в JSON
        /// </summary>
        /// <param name="list">Список</param>
        /// <returns></returns>
        public static string ToJSON(this IReadOnlyList<int> list)
        {
            var builder = new StringBuilder();
            builder.Append('[');
            for (int i = 0; i < list.Count - 1; i++)
            {
                builder.Append(list[i].ToString());
                builder.Append(", ");
            }
            builder.Append(list[list.Count - 1]);
            builder.Append("]");
            return builder.ToString();
        }
        public static IEnumerable<Control> GetAllDescendantControls(this Control c)
        {
            return c.Controls.Cast<Control>()
                .Concat(c.Controls.Cast<Control>().SelectMany(x => x.GetAllDescendantControls()));
        }
        /// <summary>
        /// Расчет корреляции значений 2-х списков.
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns></returns>
        public static double Correlation(this List<double> list1, List<double> list2)
        {
            double Sum1 = 0, Sum2 = 0, Sum3 = 0;
            double avg1 = list1.Average(), avg2 = list2.Average();
            for (int j = 0; j < list1.Count; j++)
            {
                Sum1 += (list1[j] - avg1) * (list2[j] - avg2);
                Sum2 += Math.Pow(list1[j] - avg1, 2);
                Sum3 += Math.Pow(list2[j] - avg2, 2);
            }
            return Sum1 * 1.0 / Math.Sqrt(Sum2 * Sum3);
        }
    }
}