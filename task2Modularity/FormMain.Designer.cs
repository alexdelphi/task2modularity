﻿namespace task2Modularity
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabStatistics = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxCorrelation = new System.Windows.Forms.TextBox();
            this.textBoxClientsProcessed = new System.Windows.Forms.TextBox();
            this.textBoxModellingTime = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabInputParams = new System.Windows.Forms.TabPage();
            this.checkBoxLoggingEnabled = new System.Windows.Forms.CheckBox();
            this.progressBarModelling = new System.Windows.Forms.ProgressBar();
            this.buttonStartModelling = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.numericTextBoxTaskCount = new task2Modularity.NumericTextBox();
            this.numericTextBoxDeterminedAmount = new task2Modularity.NumericTextBox();
            this.numericTextBoxBetaA = new task2Modularity.NumericTextBox();
            this.numericTextBoxExponentialLambda = new task2Modularity.NumericTextBox();
            this.numericTextBoxBetaB = new task2Modularity.NumericTextBox();
            this.numericTextBoxScale = new task2Modularity.NumericTextBox();
            this.groupBox6.SuspendLayout();
            this.tabStatistics.SuspendLayout();
            this.tabInputParams.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.numericTextBoxDeterminedAmount);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.numericTextBoxBetaA);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.numericTextBoxExponentialLambda);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.numericTextBoxBetaB);
            this.groupBox6.Controls.Add(this.numericTextBoxScale);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox6.ForeColor = System.Drawing.Color.Blue;
            this.groupBox6.Location = new System.Drawing.Point(8, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(503, 195);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Параметры потоков";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(6, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 20);
            this.label1.TabIndex = 17;
            this.label1.Text = "λ:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(277, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(197, 20);
            this.label11.TabIndex = 7;
            this.label11.Text = "Постоянный поток данных";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(6, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Бета-распределение";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(277, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "Значение:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(276, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(159, 20);
            this.label9.TabIndex = 5;
            this.label9.Text = "Распределение Рэлея";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(6, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(256, 20);
            this.label12.TabIndex = 9;
            this.label12.Text = "Экспоненциальное распределение";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "A:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(83, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "B:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(277, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Масштаб:";
            // 
            // tabStatistics
            // 
            this.tabStatistics.Controls.Add(this.label17);
            this.tabStatistics.Controls.Add(this.textBoxCorrelation);
            this.tabStatistics.Controls.Add(this.textBoxClientsProcessed);
            this.tabStatistics.Controls.Add(this.textBoxModellingTime);
            this.tabStatistics.Controls.Add(this.label16);
            this.tabStatistics.Controls.Add(this.label14);
            this.tabStatistics.Location = new System.Drawing.Point(4, 29);
            this.tabStatistics.Name = "tabStatistics";
            this.tabStatistics.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatistics.Size = new System.Drawing.Size(517, 320);
            this.tabStatistics.TabIndex = 1;
            this.tabStatistics.Text = "Статистика";
            this.tabStatistics.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 20);
            this.label17.TabIndex = 7;
            this.label17.Text = "Корреляция:";
            // 
            // textBoxCorrelation
            // 
            this.textBoxCorrelation.Location = new System.Drawing.Point(188, 80);
            this.textBoxCorrelation.Name = "textBoxCorrelation";
            this.textBoxCorrelation.ReadOnly = true;
            this.textBoxCorrelation.Size = new System.Drawing.Size(100, 27);
            this.textBoxCorrelation.TabIndex = 6;
            // 
            // textBoxClientsProcessed
            // 
            this.textBoxClientsProcessed.Location = new System.Drawing.Point(188, 47);
            this.textBoxClientsProcessed.Name = "textBoxClientsProcessed";
            this.textBoxClientsProcessed.ReadOnly = true;
            this.textBoxClientsProcessed.Size = new System.Drawing.Size(100, 27);
            this.textBoxClientsProcessed.TabIndex = 5;
            // 
            // textBoxModellingTime
            // 
            this.textBoxModellingTime.Location = new System.Drawing.Point(188, 14);
            this.textBoxModellingTime.Name = "textBoxModellingTime";
            this.textBoxModellingTime.ReadOnly = true;
            this.textBoxModellingTime.Size = new System.Drawing.Size(100, 27);
            this.textBoxModellingTime.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(167, 20);
            this.label16.TabIndex = 2;
            this.label16.Text = "Клиентов обработано:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 20);
            this.label14.TabIndex = 0;
            this.label14.Text = "Время работы сервера:";
            // 
            // tabInputParams
            // 
            this.tabInputParams.Controls.Add(this.checkBoxLoggingEnabled);
            this.tabInputParams.Controls.Add(this.progressBarModelling);
            this.tabInputParams.Controls.Add(this.buttonStartModelling);
            this.tabInputParams.Controls.Add(this.numericTextBoxTaskCount);
            this.tabInputParams.Controls.Add(this.label13);
            this.tabInputParams.Controls.Add(this.label7);
            this.tabInputParams.Controls.Add(this.groupBox6);
            this.tabInputParams.Location = new System.Drawing.Point(4, 29);
            this.tabInputParams.Name = "tabInputParams";
            this.tabInputParams.Padding = new System.Windows.Forms.Padding(3);
            this.tabInputParams.Size = new System.Drawing.Size(517, 320);
            this.tabInputParams.TabIndex = 0;
            this.tabInputParams.Text = "Входные параметры";
            this.tabInputParams.UseVisualStyleBackColor = true;
            // 
            // checkBoxLoggingEnabled
            // 
            this.checkBoxLoggingEnabled.AutoSize = true;
            this.checkBoxLoggingEnabled.Location = new System.Drawing.Point(18, 246);
            this.checkBoxLoggingEnabled.Name = "checkBoxLoggingEnabled";
            this.checkBoxLoggingEnabled.Size = new System.Drawing.Size(227, 24);
            this.checkBoxLoggingEnabled.TabIndex = 16;
            this.checkBoxLoggingEnabled.Text = "Включить запись в лог-файл";
            this.checkBoxLoggingEnabled.UseVisualStyleBackColor = true;
            // 
            // progressBarModelling
            // 
            this.progressBarModelling.Location = new System.Drawing.Point(18, 281);
            this.progressBarModelling.Name = "progressBarModelling";
            this.progressBarModelling.Size = new System.Drawing.Size(486, 36);
            this.progressBarModelling.TabIndex = 15;
            // 
            // buttonStartModelling
            // 
            this.buttonStartModelling.Location = new System.Drawing.Point(288, 239);
            this.buttonStartModelling.Name = "buttonStartModelling";
            this.buttonStartModelling.Size = new System.Drawing.Size(216, 36);
            this.buttonStartModelling.TabIndex = 14;
            this.buttonStartModelling.Text = "Начать моделирование";
            this.buttonStartModelling.UseVisualStyleBackColor = true;
            this.buttonStartModelling.Click += new System.EventHandler(this.buttonStartModelling_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(14, 210);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(155, 20);
            this.label13.TabIndex = 10;
            this.label13.Text = "Количество заданий:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 230);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 20);
            this.label7.TabIndex = 7;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabInputParams);
            this.tabControlMain.Controls.Add(this.tabStatistics);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(525, 353);
            this.tabControlMain.TabIndex = 0;
            // 
            // numericTextBoxTaskCount
            // 
            this.numericTextBoxTaskCount.Location = new System.Drawing.Point(378, 207);
            this.numericTextBoxTaskCount.Name = "numericTextBoxTaskCount";
            this.numericTextBoxTaskCount.Size = new System.Drawing.Size(126, 27);
            this.numericTextBoxTaskCount.TabIndex = 11;
            this.numericTextBoxTaskCount.Text = "0";
            this.numericTextBoxTaskCount.Value = 0D;
            // 
            // numericTextBoxDeterminedAmount
            // 
            this.numericTextBoxDeterminedAmount.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericTextBoxDeterminedAmount.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericTextBoxDeterminedAmount.Location = new System.Drawing.Point(371, 151);
            this.numericTextBoxDeterminedAmount.Name = "numericTextBoxDeterminedAmount";
            this.numericTextBoxDeterminedAmount.Size = new System.Drawing.Size(125, 27);
            this.numericTextBoxDeterminedAmount.TabIndex = 3;
            this.numericTextBoxDeterminedAmount.Text = "0";
            this.numericTextBoxDeterminedAmount.Value = 0D;
            // 
            // numericTextBoxBetaA
            // 
            this.numericTextBoxBetaA.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericTextBoxBetaA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericTextBoxBetaA.Location = new System.Drawing.Point(34, 46);
            this.numericTextBoxBetaA.Name = "numericTextBoxBetaA";
            this.numericTextBoxBetaA.Size = new System.Drawing.Size(43, 27);
            this.numericTextBoxBetaA.TabIndex = 3;
            this.numericTextBoxBetaA.Text = "0";
            this.numericTextBoxBetaA.Value = 0D;
            // 
            // numericTextBoxExponentialLambda
            // 
            this.numericTextBoxExponentialLambda.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericTextBoxExponentialLambda.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericTextBoxExponentialLambda.Location = new System.Drawing.Point(34, 147);
            this.numericTextBoxExponentialLambda.Name = "numericTextBoxExponentialLambda";
            this.numericTextBoxExponentialLambda.Size = new System.Drawing.Size(126, 27);
            this.numericTextBoxExponentialLambda.TabIndex = 8;
            this.numericTextBoxExponentialLambda.Text = "0";
            this.numericTextBoxExponentialLambda.Value = 0D;
            // 
            // numericTextBoxBetaB
            // 
            this.numericTextBoxBetaB.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericTextBoxBetaB.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericTextBoxBetaB.Location = new System.Drawing.Point(109, 46);
            this.numericTextBoxBetaB.Name = "numericTextBoxBetaB";
            this.numericTextBoxBetaB.Size = new System.Drawing.Size(51, 27);
            this.numericTextBoxBetaB.TabIndex = 2;
            this.numericTextBoxBetaB.Text = "0";
            this.numericTextBoxBetaB.Value = 0D;
            // 
            // numericTextBoxScale
            // 
            this.numericTextBoxScale.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericTextBoxScale.ForeColor = System.Drawing.SystemColors.ControlText;
            this.numericTextBoxScale.Location = new System.Drawing.Point(370, 46);
            this.numericTextBoxScale.Name = "numericTextBoxScale";
            this.numericTextBoxScale.Size = new System.Drawing.Size(126, 27);
            this.numericTextBoxScale.TabIndex = 3;
            this.numericTextBoxScale.Text = "0";
            this.numericTextBoxScale.Value = 0D;
            // 
            // FormMain
            // 
            this.AcceptButton = this.buttonStartModelling;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 353);
            this.Controls.Add(this.tabControlMain);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.Text = "Задача 2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabStatistics.ResumeLayout(false);
            this.tabStatistics.PerformLayout();
            this.tabInputParams.ResumeLayout(false);
            this.tabInputParams.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label11;
        private NumericTextBox numericTextBoxDeterminedAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private NumericTextBox numericTextBoxBetaA;
        private System.Windows.Forms.Label label9;
        private NumericTextBox numericTextBoxBetaB;
        private NumericTextBox numericTextBoxScale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabStatistics;
        private System.Windows.Forms.TabPage tabInputParams;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.Label label12;
        private NumericTextBox numericTextBoxExponentialLambda;
        private System.Windows.Forms.Label label7;
        private NumericTextBox numericTextBoxTaskCount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonStartModelling;
        private System.Windows.Forms.ProgressBar progressBarModelling;
        private System.Windows.Forms.CheckBox checkBoxLoggingEnabled;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxModellingTime;
        private System.Windows.Forms.TextBox textBoxClientsProcessed;
        private System.Windows.Forms.TextBox textBoxCorrelation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
    }
}