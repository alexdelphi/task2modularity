﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;
using task2Modularity.RandomGenerators;
using System.Diagnostics;

namespace task2Modularity
{
    public partial class FormMain : Form
    {
        // "Изменение тика". Реально отражается на параметрах каждого распределения (все тики целочисленные)
        private double _deltaTick = 0;
        // Дефолтное изменение тика (если мы из XM получили 0)
        private const double _defaultDeltaTick = 0.1;
        // Дефолтное количество заданий
        private const int _defaultTaskCount = 100;
        private const string config = "config.xml";
        public FormMain()
        {
            InitializeComponent();            
        }
        /// <summary>
        /// Сохраняет все данные в Xml
        /// </summary>
        public void SaveToXmlConfig(string configFileName)
        {
            var document = new XDocument();
            var controlValues = new List<XElement>();
            // В конфиг запихаем все входные параметры
            foreach (var numeric in this.GetAllDescendantControls().OfType<NumericTextBox>())
            {
                controlValues.Add(new XElement(numeric.Name, numeric.Text));
            }
            var controlsElement = new XElement("controlValues", controlValues);
            // и про параметр логирования не забудем
            var loggingEnabledNode = new XElement("loggingEnabled", checkBoxLoggingEnabled.Checked);
            var deltaXml = new XElement("delta", _deltaTick);
            var rootElement = new XElement("configs", controlsElement);
            rootElement.Add(loggingEnabledNode);
            rootElement.Add(deltaXml);
            document.Add(rootElement);
            using (var writer = new StreamWriter(configFileName))
            {
                document.Save(writer);
                writer.Close();
            }
        }
        private void buttonStartModelling_Click(object sender, EventArgs e)
        {
            if (_deltaTick == 0)
            {
                _deltaTick = _defaultDeltaTick;
            }
            int taskCount = Convert.ToInt32(numericTextBoxTaskCount.Value);
            if (taskCount == 0)
            {
                taskCount = _defaultTaskCount;
            }
        
            var workingTime = DateTime.Now;
            Logger.Instance.Enabled = checkBoxLoggingEnabled.Checked;
            // Тип ресурса/количество модулей ("Варианты характеристик компьютерной системы").
            // От индексов всех ресурсов отнимается 1
            var moduleAmounts = new Dictionary<int, int> {
                    {0, 2},
                    {1, 1},
                    {2, 1},
                    {3, 2}
            };
            // "Варианты потоков заданий". Дополнительно "растягиваем" интервалы
            var resourceFlow = new List<Resource> {
                    new Resource(
                            0, new StandardRandom().LinearTransform(0, 1.0 / _deltaTick)
                    ),
                    new Resource(
                            2, new Beta(
                                    Convert.ToDouble(numericTextBoxBetaA.Value), Convert.ToDouble(numericTextBoxBetaB.Value)
                            ).LinearTransform(0, 1.0 / _deltaTick)
                    ),
                    new Resource(
                            0, new Determined(Convert.ToDouble(numericTextBoxDeterminedAmount.Value)).LinearTransform(0, 1.0 / _deltaTick)
                    ),
                    new Resource(
                            2, new Beta(
                                    Convert.ToDouble(numericTextBoxBetaA.Value), Convert.ToDouble(numericTextBoxBetaB.Value)
                            ).LinearTransform(0, 1.0 / _deltaTick)
                    ),
                    new Resource(
                            0, new Rayleigh(
                                    Convert.ToDouble(numericTextBoxScale.Value)
                            ).LinearTransform(0, 1.0 / _deltaTick)
                    )
            };
            // Задание на поступление клиентов
            var clientArrivalTime = new Exponential(Convert.ToDouble(numericTextBoxExponentialLambda.Value)).LinearTransform(0, 1.0 / _deltaTick);
            var server = new Server(moduleAmounts, resourceFlow, clientArrivalTime);
            server.RunWorkerCompleted += (s, args) => buttonStartModelling.Invoke(new Action(() =>
            {
                textBoxModellingTime.Text = (DateTime.Now - workingTime).ToString(@"mm\:ss\.fff");
                textBoxClientsProcessed.Text = taskCount.ToString();
                if (Logger.Instance.Enabled)
                {
                    foreach (var client in server.Clients)
                    {
                        Logger.Instance.WriteLine(client.GetDebugInfo());
                    }
                }
                // Расчет корреляции
                var ClientsProcessingTime = new List<double>();
                var ClientsWaitingTime = new List<double>();
                foreach (var client in server.Clients)
                {
                    double sum = 0;
                    for (int i = 0; i != client.TicksProcessing.Count; i++)
                    {
                        sum += client.TicksProcessing[i] * 1.0 / moduleAmounts[resourceFlow[i].Type];
                    }
                    ClientsProcessingTime.Add(sum);
                    ClientsWaitingTime.Add(client.TicksWaiting * 1.0);
                }
                textBoxCorrelation.Text = ClientsProcessingTime.Correlation(ClientsWaitingTime).ToString();
                buttonStartModelling.Enabled = true;
                tabControlMain.SelectTab(tabStatistics);
            }));
            server.ProgressChanged += (s, state) => progressBarModelling.Invoke(new Action(() => 
            {
                progressBarModelling.Value = state.ProgressPercentage;
            }));
            SaveToXmlConfig(config);
            // поехали!
            progressBarModelling.Value = 0;
            buttonStartModelling.Enabled = false;
            try
            {
                server.ProcessTasks(taskCount);
            }
            finally
            {
                Logger.Instance.Flush();
            }
        }
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveToXmlConfig(config);
            Logger.Instance.Dispose();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            if (!File.Exists(config))
            {
                return;
            }
            using (var reader = new StreamReader(config))
            {
                var document = XDocument.Parse(reader.ReadToEnd());
                var controlValues = document.Root.Elements("controlValues").First().Elements().ToList();
                var controls = this.GetAllDescendantControls().OfType<NumericTextBox>().ToList();
                for (int i = 0; i != controls.Count(); i++)
                {
                    controls[i].Value = Convert.ToDouble(controlValues[i].Value);
                }
                var loggingNodes = document.Root.Elements("loggingEnabled");
                if (loggingNodes.Count() != 0)
                {
                    checkBoxLoggingEnabled.Checked = Convert.ToBoolean(loggingNodes.First().Value);
                }
                var deltaXml = document.Root.Elements("deltaTick");
                if (deltaXml != null)
                {
                    _deltaTick = Convert.ToInt32(deltaXml.First().Value);
                }
                reader.Close();
            }
        }
    }
}
