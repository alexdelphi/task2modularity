﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace task1Modelling
{
    public static class ChartHelper
    {
        /// <summary>
        /// Показывает гистограмму цветом на графике
        /// </summary>
        /// <param name="chart"></param>
        /// <param name="color"></param>
        public static void ShowChartData(this IEnumerable<IChartPoint> _this, Chart chart, string name, Color color)
        {
            chart.Series.Clear();
            chart.Series.Add(new Series
            {
                Name = name,
                ChartType = SeriesChartType.Column,
                MarkerStyle = MarkerStyle.Diamond,
                MarkerSize = 0,
                Color = color
            });
            //chart.ChartAreas[0].AxisX.IsMarginVisible = false;
            foreach (var part in _this)
            {
                //Logger.Instance.WriteLine(string.Format("{0}: {1} {2}", name, part.GetChartLabel(), part.GetChartValue()));
                chart.Series[0].Points.AddXY(part.GetChartLabel(), part.GetChartValue());
            }
            chart.ChartAreas[0].RecalculateAxesScale();
        }
    }
}
