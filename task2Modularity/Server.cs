﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using task2Modularity.RandomGenerators;

namespace task2Modularity
{
    /// <summary>
    /// Периодичность появления события
    /// </summary>
    public enum OccurrencePattern
    {
        /// <summary>
        /// Однократное появление события
        /// </summary>
        JustOnce,
        /// <summary>
        /// Периодическое появления события
        /// </summary>
        Periodic
    };

    /// <summary>
    /// Сервер/вычислительная модель
    /// </summary>
    public partial class Server : BackgroundWorker
    {
        // количество задач, которые надо обработать
        private int _taskCount = 0;
        /// <summary>
        /// Общее количество тиков
        /// </summary>
        public int TicksTotal { get; private set; }

        // Различные обработчики тиков, срабатывающие каждый "случайный" тик
        private readonly DeferredEventCollection _deferredEvents = new DeferredEventCollection();
        // Клиенты, ждущие конкретного ресурса
        private readonly Dictionary<Module, Queue<Client>> _clientQueues = new Dictionary<Module, Queue<Client>>();
        // Задание на поступление клиента
        private readonly DeferredEvent _clientArrivalTask;
        // Просто клиенты :)
        private readonly List<Client> _clients = new List<Client>();
        // тип модуля/количество модулей в ресурсе
        private readonly List<Resource> _resources;
        // Количество клиентов, обработка которых завершена
        private int _clientsFinished = 0;
        // Остановился ли сервер
        private bool _isStopped = false;
        // Процент выполнения задания.
        // При завершении задачи проверяется сначала он, чтобы не гонять новый поток
        private int _percentage = 0;
        /// <summary>
        /// Коллекция клиентов
        /// </summary>
        public IReadOnlyList<Client> Clients
        {
            get
            {
                return _clients;
            }
        }
        // все модули сервера
        private readonly List<Module> _modules;
        // Время, через которое приходят новые клиенты
        private readonly RandomDistribution _clientArrivalTime;

        private void Tick()
        {
            TicksTotal++; // Общий счетчик тиков
            _deferredEvents.Tick(); // Отложенные события
        }

        /// <summary>
        /// Возникает по окончании обработки очередной задачи
        /// </summary>        
        public event Action<int> TaskProcessed;
        protected virtual void OnTaskProcessed(int tasks)
        {
            TaskProcessed(tasks);
        }
        /// <summary>
        /// Создать новый сервер
        /// </summary>
        /// <param name="moduleAmounts">"Варианты характеристик компьютерной системы"</param>
        /// <param name="resources">"Варианты потоков заданий" (от номеров всех ресурсов отнимается 1)</param>
        /// <param name="clientArrivalTime">Распределение прихода клиентов</param>
        /// <param name="deltaTick">Изменение тика</param>
        internal Server(
                Dictionary<int, int> moduleAmounts,
                List<Resource> resources,
                RandomDistribution clientArrivalTime
        )
        {
            _modules = new List<Module>();
            foreach (var moduleAmount in moduleAmounts)
            {
                _modules.Add(new Module(moduleAmount.Key, moduleAmount.Value));
            }
            _resources = resources;
            _clientArrivalTime = clientArrivalTime;
            // Инициализируем очереди клиентов к модулям
            foreach (var module in _modules)
            {
                _clientQueues.Add(module, new Queue<Client>());
            }
            // Задание на поступление клиентов
            _clientArrivalTask = new DeferredEvent(
                clientArrivalTime, () =>
                {
                    // Ограничение на количество клиентов
                    if (_clients.Count == _taskCount - 1)
                    {
                        _deferredEvents.Remove(_clientArrivalTask);
                    }
                    var client = new Client(
                        _clients.Count + 1,
                        () => { return TicksTotal; } // системное время
                    );
                    client.Finished += () =>
                    {
                        _clientsFinished++;
                        Logger.Instance.WriteLine(string.Format("Client № {0} fully processed. Ticks: {1}", client.Id, TicksTotal));
                        // обновляем инфу на прогресс-баре.
                        int currentPercentage = Convert.ToInt32(Math.Round(100.0 * _clientsFinished / _taskCount));
                        if (currentPercentage != _percentage) 
                        {
                            _percentage = currentPercentage;
                            OnProgressChanged(new ProgressChangedEventArgs(_percentage, null));
                        }
                        if (_clientsFinished == _taskCount)
                        {
                            _isStopped = true;
                        }
                    };
                    client.Processed += () =>
                    {
                        Logger.Instance.WriteLine(string.Format("Client {0} processed module {1}", client.Id, client.ModuleId)); // здесь все норм
                    };
                    _clients.Add(client);
                    Process(client);
                }, OccurrencePattern.Periodic);
            _deferredEvents.Add(_clientArrivalTask);
        }
        /// <summary>
        /// Вытолкнуть клиентов из очереди, если есть свободные модули
        /// </summary>
        /// <param name="state"></param>
        /// <param name="taskCount"></param>
        private void PullClientsFromQueue()
        {
            foreach (var clientQueue in _clientQueues)
            {
                // Если какому-то модулю нечего делать, подсовываем ему клиента из очереди
                if ((!clientQueue.Key.Occupied) && (clientQueue.Value.Count != 0))
                {
                    Client clientToProcess = clientQueue.Value.Dequeue();
                    clientToProcess.PulledFromQueue();
                    Logger.Instance.WriteLine(string.Format("Pulled client {0} from queue", clientToProcess.Id));
                    // Обрабатываем один модуль.
                    // После этого цикл обработки "запустится" сам в соответствии с методом Process().
                    ProcessCurrentModule(clientToProcess);
                }
            }
            Logger.Instance.WriteLine("Tried pulling elements from queue; no one waiting");
        }
        /// <summary>
        /// Запустить обработку всех задач
        /// </summary>
        /// <param name="taskCount"></param>
        public void ProcessTasks(int taskCount)
        {
            _taskCount = taskCount;
            base.DoWork += (sender, args) =>
            {
                while (!_isStopped)
                {
                    PullClientsFromQueue();
                    Tick();
                    Logger.Instance.WriteLine("TICKS: " + TicksTotal);
                }
            };
            base.RunWorkerAsync();
        }
        /// <summary>
        /// Начать обработку клиентом всех ресурсов
        /// </summary>
        /// <param name="client">Клиент</param>
        private void Process(Client client)
        {
            // Очевидно, что циклы мы использовать не можем, 
            // поскольку "итерации" исполняются не подряд.
            // Вместо этого контролируем вход/выход в обработчик вручную.
            // Если обработка только началась, подписываемся на событие.
            if (client.ModuleId == 0)
            {
                client.Processed += () =>
                {
                    Process(client);
                };
            }
            // Если обработка закончилась, выходим              
            if (client.ModuleId == _resources.Count - 1)
            {
                client.Processed -= () => 
                { 
                    Process(client); 
                };
                client.WorkFinished();
                return;
            }            
            ProcessCurrentModule(client);
        }
        /// <summary>
        /// Начать обработку клиентом текущего модуля
        /// </summary>
        /// <param name="client">Клиент</param>
        public void ProcessCurrentModule(Client client)
        {
            var currentResource = _resources[client.ModuleId];
            var currentModule = _modules[currentResource.Type];
            Logger.Instance.WriteLine(currentModule.FreeResources.ToString());
            // если модуль занят, становимся в очередь
            if (currentModule.Occupied)
            {
                // В очередь!
                _clientQueues[currentModule].Enqueue(client);
                client.State = ClientState.Waiting;
                return;
            }
            // Начинаем обработку. По окончании обработки освобождаем модуль и сообщаем клиенту
            _deferredEvents.Add(new DeferredEvent(currentResource.Distribution, () =>
            {
                client.CurrentModuleFinished();
                currentModule.Free();
            }, OccurrencePattern.JustOnce));
            client.StartProcessing();
            currentModule.Occupy();
        }
    }
}