﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    /// <summary>
    /// Стандартный рандом от 0 до 1
    /// </summary>
    class StandardRandom : RandomDistribution
    {        
        public override double FromEvenRandom(double num)
        {
            return num;
        }
    }
}
