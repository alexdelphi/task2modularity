﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    public class Exponential : RandomDistribution
    {
        public double Lambda { get; private set; }
        public Exponential(double lambda)
        {
            Lambda = lambda;
        }
        public override double FromEvenRandom(double num)
        {
            return (num > 0) ? Lambda * Math.Exp(-Lambda * num) : 0;
        }
    }
}
