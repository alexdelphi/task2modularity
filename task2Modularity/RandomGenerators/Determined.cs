﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    /// <summary>
    /// Детерминированный "генератор случайных чисел".
    /// Всегда возвращает одно значение.
    /// </summary>
    class Determined : RandomDistribution
    {
        public double Amount { get; private set; }
        public Determined(double amount)
        {
            Amount = amount;
        }
        public override double FromEvenRandom(double num)
        {
            throw new NotImplementedException();
        }

        public override double NextDouble()
        {
            return Amount;
        }
    }
}
