﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    public class Erlang : RandomDistribution
    {
        public double Alpha { get; private set; }
        public double K { get; private set; }
        public Erlang(double alpha = 0, double k = 1)
        {
            Alpha = alpha;
            K = k;
        }
        public override double FromEvenRandom(double num)
        {
            double sum = 0;
            int currentFact = 1; // текущий факториал
            double currentPower = 1; // (a*x)^tabIndex
            for (int i = 0; i < K; i++)
            {
                sum += currentPower * 1.0 / currentFact;
                currentPower *= Alpha * num;
                currentFact *= (i + 1);
            }
            sum *= Math.Exp(-Alpha * num);
            return 1.0 - sum;
        }
    }
}
