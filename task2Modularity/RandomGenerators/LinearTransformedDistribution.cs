﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity
{
    public abstract partial class RandomDistribution
    {
        private class LinearTransformedDistribution : RandomDistribution
        {
            private readonly RandomDistribution _baseDistribution;
            /// <summary>
            /// Нижняя граница
            /// </summary>
            private readonly double _lowerBound;
            /// <summary>
            /// Верхняя граница
            /// </summary>
            private readonly double _upperBound;
            public LinearTransformedDistribution(RandomDistribution baseDistribution, double lowerBound, double upperBound)
            {
                _baseDistribution = baseDistribution;
                _lowerBound = lowerBound;
                _upperBound = upperBound;
            }
            public override double FromEvenRandom(double num)
            {
                return _baseDistribution.FromEvenRandom(num) * (_upperBound - _lowerBound) + _lowerBound;
            }
            public override double NextDouble()
            {
                return _baseDistribution.NextDouble() * (_upperBound - _lowerBound) + _lowerBound;
            }
        }
        /// <summary>
        /// Выдает новое распределение со значениями, отображенными на промежуток от нижней до верхней границы.
        /// Предназначена для применения с распределениями, возвращающими значения в интервале [0; 1].
        /// </summary>
        /// <param name="lowerBound">Нижняя граница</param>
        /// <param name="upperBound">Верхняя граница</param>
        /// <returns></returns>
        public RandomDistribution LinearTransform(double lowerBound, double upperBound)
        {
            return new LinearTransformedDistribution(this, lowerBound, upperBound);
        }
    }
}
