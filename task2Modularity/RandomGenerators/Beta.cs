﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    /// <summary>
    /// Бета-распределение оттуда: http://algolist.manual.ru/maths/matstat/beta/index.php
    /// </summary>
    class Beta : RandomDistribution
    {
        public double A { get; private set; }
        public double B { get; private set; }
        public Beta(double a, double b)
        {
            A = a;
            B = b;
        }
        public override double FromEvenRandom(double num)
        {
            throw new NotImplementedException();
        }
        public override double NextDouble()
        {
            double r1, r2;
            for (; ; )
            {
                r1 = Math.Pow(_random.NextDouble(), 1.0 / A);
                r2 = Math.Pow(_random.NextDouble(), 1.0 / B);
                if (r1 + r2 < 1)
                {
                    return r1 * 1.0 / (r1 + r2);
                }
            }
        }
    }
}
