﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    /// <summary>
    /// Распределение Рэлея для неотрицательных чисел
    /// </summary>
    class Rayleigh : RandomDistribution
    {
        public double Scale { get; private set; }
        public Rayleigh(double scale)
        {
            Scale = scale;
        }
        public override double FromEvenRandom(double num)
        {
            if (num >= 0)
            {
                return 1.0 - Math.Exp(-0.5 * Math.Pow(num * 1.0 / Scale, 2));
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}
