﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    // Полетит - переделаю гаусса
    /// <summary>
    /// Логнормальное распределение; логарифм от гаусса
    /// </summary>
    class LogNormal : Gauss
    {
        public override double FromEvenRandom(double num)
        {
            double value = base.FromEvenRandom(num);
            if (value < 0)
                throw new ArgumentOutOfRangeException();
            return (value >= 1) ? Math.Log(value) : 0;
        }
    }
}
