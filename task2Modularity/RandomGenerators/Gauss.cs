﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2Modularity.RandomGenerators
{
    /// <summary>
    /// Позволяет создать случайную переменную с гауссовым распределением.
    /// Во всех вызовах методов avg - матожидание, deviation - среднеквадратичное отклонение
    /// </summary>
    public class Gauss : RandomDistribution
    {
        public double Avg { get; private set; }
        public double Deviation { get; private set; }
        public Gauss(double avg = 0, double deviation = 1)
        {
            Avg = avg;
            Deviation = deviation;
        }
        // Дефолтные значения параметров соответствуют стандартным изменениям
        public override double FromEvenRandom(double num)
        {
            double exponent = Math.Pow((num - Avg) / Deviation, 2) / -2.0;
            return Math.Exp(exponent) / (Deviation * Math.Sqrt(2 * Math.PI));
        }
    }
}
